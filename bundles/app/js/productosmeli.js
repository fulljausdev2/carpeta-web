		$("#tabla-ajax").on('click', ".onoffswitch-estatus", function(e) {
			e.preventDefault();
			var input = $(this).children('input[id^="check_estatus_"]') //attr('data-id');
			var id=input.attr('data-id')
			if (input.is(':checked') ) {
				input.val('0');
				input.prop('checked', false);
				aplicarEstado(id,input.val());
			} else {
				input.val('1');
				input.prop('checked', true);
				aplicarEstado(id, input.val());
			}
		});

		